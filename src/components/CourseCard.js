import { useState , useEffect} from "react";
import { Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function CourseCard({course}) {

    const {name, description, price, _id} = course

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Syntax
        // const [getter, setter] = useState(initialGetterValue);
    const [count , setCount] = useState(0);
    const [seats, setSeats] = useState(30)
    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element

    // console.log(useState(0))

    // function enroll() {
    //     if(seats <= 0) {return alert( `no more seats for course: ${name}`)}
    //     setCount(count + 1)
    //     setSeats(seats - 1)
    //     console.log(`Enrollees : ${count}`)
    // }

    // useEffect(() => {
    //     if(seats === 0){
    //         setSeats(false)
    //         // alert('No more seats')
    //     }
    // }, [seats])

    return (
        <Card className="">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>PhP {price.toLocaleString('en-US')}</Card.Text>
                {/* <Card.Text>Enrollees: {count}</Card.Text> */}
                {/* <Card.Text>Seats: {seats}</Card.Text> */}
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}