import { Row, Col, Card } from "react-bootstrap"

export default function Hightlights() {
    return (

        <Row className="mt-3 mb-3">
            <Col xs={12} md={4}>
                <Card className="cardHightlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Learn from Home</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse a felis id ligula sodales gravida in ac dolor. Vestibulum vitae.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className="cardHightlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Study Now, Pay Later</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse a felis id ligula sodales gravida in ac dolor. Vestibulum vitae.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className="cardHightlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Be Part of Our Community</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse a felis id ligula sodales gravida in ac dolor. Vestibulum vitae.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>

    )
}