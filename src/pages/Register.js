import {Form, Button} from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import { Navigate, useNavigate } from 'react-router-dom'
import Swal from "sweetalert2";
import UserContext from '../UserContext'

export default function Register() {

    const {user} = useContext(UserContext)

    const navigate = useNavigate()

    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [email, setEmail] = useState("")
    const [mobileNumber, setMobileNumber] = useState("")
    const [password1, setPassword1] = useState("")
    const [password2, setPassword2] = useState("")
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    // console.log(email)
    // console.log(password1)
    // console.log(password2)

    function registerUser(e) {
        e.preventDefault();

        // CHECK FOR DUPLICATE EMAIL
        fetch(`${process.env.REACT_APP_API_URL}users/checkEmail`,
        {
            headers: {
                "Content-Type" : "application/json",
            },
            method: "POST",
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            
            if(data){
                // If Email already exists then 
                //Throw an Error that will be caught by catch
                const err = new Error ('Duplicate email found')
                err.message2 = 'Please provide a different email'
                throw err
            } else {
                // If email is valid then make fetch request to 
                // register the user
                return fetch(`${process.env.REACT_APP_API_URL}users/register`,
                {
                method: 'POST',
                headers: {
                    "Content-Type" : "application/json",
                },
                body: JSON.stringify(
                    {
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        password: password1,
                        mobileNo: mobileNumber
                    }
                )
                })
            }
        })
        .then(res => res.json())
        .then(registerSuccess => {
            if(registerSuccess){
                Swal.fire({
                    icon: 'success',
                    title: 'Registration successful',
                    text: 'Welcome to Zuitt',
                  })
                navigate('/login')   
            }
        })
        .catch(err => {
            Swal.fire({
                icon: 'error',
                title: err.message,
                text: err.message2 ? err.message2 : "",
              })
        })
        


        

        // REGISTER USER


        // setEmail('')
        // setPassword1('')
        // setPassword2('')
        // alert('Thank you for registering!')
    }

    useEffect(() => {
        if((
            firstName !== "" &&
            lastName !== "" &&
            email !== "" &&
            mobileNumber !== "" &&
            mobileNumber.length > 11 &&
            password1 !== "" &&
            password2 !=="") &&
           (password1 === password2)){
            setIsActive(true)
        }else {
            setIsActive(false)
        }
    }, [firstName, lastName, email, mobileNumber, password1, password2])
    

    // if(user.email !== null) {
        //     return (
            //         <Navigate to='/courses' />
            //     )
            // }
            
    return (
        // If user is logged in, navigate to courses page
        (user.id) ?
            <Navigate to='/courses' />
        :
        <Form onSubmit={(e) => registerUser(e)}>
        
            <Form.Group controlId='firstName'>
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text"
                    placeholder="Enter first name"
                    value={firstName}
                    onChange={evt => setFirstName(evt.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId='lastName'>
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text"
                    placeholder="Enter last name"
                    value={lastName}
                    onChange={evt => setLastName(evt.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId='userEmail'>
                <Form.Label>Email Address</Form.Label>
                <Form.Control 
                    type="email"
                    placeholder="email@mail.com"
                    value={email}
                    onChange={evt => setEmail(evt.target.value)}
                    required
                />
                <Form.Text className="text-muted">We'll never share your email with anyone.</Form.Text>
            </Form.Group>

            <Form.Group controlId='mobileNumber'>
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="text"
                    placeholder="Enter Mobile Number"
                    value={mobileNumber}
                    onChange={evt => setMobileNumber(evt.target.value)}
                    required
                    minLength={11}
                />
            </Form.Group>

            <Form.Group controlId='password1'>
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password"
                    placeholder="Enter a password"
                    value={password1}
                    onChange={evt => setPassword1(evt.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId='password2'>
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control 
                    type="password"
                    placeholder="Confirm your password"
                    value={password2}
                    onChange={evt => setPassword2(evt.target.value)}
                    required
                />
            </Form.Group>

            {
                isActive ?
                <Button className='mt-3' variant='primary' type="submit" id="submitBtn">Submit</Button>
                :
                <Button className='mt-3' variant='danger' type="submit" id="submitBtn" disabled>Submit</Button>
            }
            
        </Form>
    )
}

// Stretch Goals
// Password and Email validation