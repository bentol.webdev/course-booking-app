import CourseCard from "../components/CourseCard";

import { useEffect, useState } from "react";
import coursesData from '../data/coursesData'

export default function Courses() {

    const [courses, setCourses] = useState([])

    // Retrieves the courses from the database upon initial render of the "Courses" component
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}courses/`)
        .then(res => res.json())
        .then(data => {
             setCourses(data.map(course => {
                return (
                    <CourseCard key={course._id} course={course}></CourseCard>
                )
            }))
        })
    }, [])

    // const courses = coursesData.map(course => {
    //     return (
    //         <CourseCard key={course.id} course={course}></CourseCard>
    //     )
    // })
    
    return (

        <>  
            {courses}
        </>
    )
}